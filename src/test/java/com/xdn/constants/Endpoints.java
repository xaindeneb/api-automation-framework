package com.xdn.constants;

public class Endpoints {
    public static final String USERS_ENDPOINT = "/api/users?page=2";
    public static final String SINGLE_USER_ENDPOINT = "/api/users/2";
    public static final String SINGLE_USER_NOT_FOUND_ENDPOINT = "/api/users/23";
}
