package com.xdn;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

public class TestBase {

    @BeforeClass
    public void setup() {
        TestBase.setBaseUri("https://reqres.in");
    }

    public static void setBaseUri(String baseUri) {
        RestAssured.baseURI = baseUri;
    }
}
