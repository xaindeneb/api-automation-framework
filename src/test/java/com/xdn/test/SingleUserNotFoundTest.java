package com.xdn.test;

import com.xdn.TestBase;
import com.xdn.constants.Endpoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class SingleUserNotFoundTest extends TestBase {

    @Test
    public void testSingleUserNotFound() {
        given().contentType(ContentType.JSON)
                .when().get(Endpoints.SINGLE_USER_NOT_FOUND_ENDPOINT)
                .then().statusCode(404);
    }
}
