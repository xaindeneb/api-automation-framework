package com.xdn.test;

import com.xdn.TestBase;
import com.xdn.constants.Endpoints;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class SingleUserTest extends TestBase {

    @Test
    public void testSingleUser() {
        given().contentType(ContentType.JSON)
                .when().get(Endpoints.SINGLE_USER_ENDPOINT)
                .then().body("data.first_name", equalTo("Janet"));
    }
}
